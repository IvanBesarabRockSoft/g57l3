//
//  VariablesAndConstants.swift
//  G57L3
//
//  Created by Ivan Vasilevich on 10/3/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class VariablesAndConstants: NSObject {

	static func printTest() {
		print("Hello G57")
	}
	
	static func greeting(count: Int) {
		for _ in 0..<count {
			printTest()
		}
	}
	
	static func countVariables() {
//		var candiesInPlate: Double = 7
//		let piNumber: Int = 3.141592
		var candiesInPlate = 7
		let piNumber = 3.141592
		print("candiesInPlate \(candiesInPlate)")
		print("I throw candy to Misha")
		var candyInMisha = 1
		candiesInPlate = candiesInPlate - 1
		print("candiesInPlate", candiesInPlate)
		print("I throw candy to Misha")
		
//		+ - * / %
		candyInMisha += 1
		candyInMisha = candyInMisha + 1
		var totalCandy = candyInMisha + candiesInPlate + Int(piNumber)
		print("totalCandy", totalCandy)
//		()
		
		let mod = 16 % 10 + 1
		print("1.5 % 3 =", mod)
	}

	static func randomChoser(variantsCount: Int = 2) {
		let numberToChoose = arc4random()%UInt32(variantsCount)
		print("choose way #\(numberToChoose)")
		
		let numberLessThen20 = numberToChoose < 20
//		< > <= >= == !=
		if numberToChoose < 10 {
			print("group1 + 1 candy")
		}
			// && 10 11 ... 19 &&
			// ||
		else if (numberLessThen20 || numberToChoose >= 10) && numberToChoose % 2 == 0 {//numberToChoose < 20 && numberToChoose >= 10 {
				print("group2 + 1 candy")
		}
		else if numberToChoose >= 20 {
			print("group3 + 1 candy")
		}
		
		var candiesInPlate = arc4random()%3
//		var candiesInPlate = arc4random()%3
		switch candiesInPlate {
		case 0:
			print("empty plate")
		case 1:
			print("OMT")
		case 2:
			print("many candy")
		default:
			print("default")
		}
	}

	static func scopeVariables() {
		var a = 5
		a = 5
		let b1 = 7
		if arc4random()%100500 > 1 {
			print("a =", a)
			let b2 = a
			print("b =", b2)
		}
		else {
			print("arc4random()%100500 < 1")
			return
		}
		
		print("b =", b1)
		a = 6
		
		
	}
	
	static func squareOfStudents(count: Int) -> Int {
		let result = count * count
		return result
		print("\(count) * \(count) = \(result)")
		return result
	}
}
