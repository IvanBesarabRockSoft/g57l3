//
//  ViewController.swift
//  G57L3
//
//  Created by Ivan Vasilevich on 10/3/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		VariablesAndConstants.printTest()
//		VariablesAndConstants.greeting(count: 5)
//		VariablesAndConstants.countVariables()
		let countOfStudentsInG57 = 20
		let countOfStudentsInG47 = 11
//		VariablesAndConstants.randomChoser(variantsCount: countOfStudentsInG57)
		let g57Square = VariablesAndConstants.squareOfStudents(count: countOfStudentsInG57)
		let g47Square = VariablesAndConstants.squareOfStudents(count: countOfStudentsInG47)
		let squareOfStudentsG57AndG47 =  g57Square + g47Square
		print("squareOfStudentsG57AndG47 = \(squareOfStudentsG57AndG47)")
		for i in 0..<10 {
			print(i)
		}
	}

}

